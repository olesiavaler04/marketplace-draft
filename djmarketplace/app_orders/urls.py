from django.urls import path

from . import views


app_name = 'orders'

urlpatterns = [
    path('create/', views.order_create, name='order_create'),
    path('order/<int:order_id>', views.order_page, name='order_page'),
    path('history/', views.OrdersHistory.as_view(), name='orders_history'),
]