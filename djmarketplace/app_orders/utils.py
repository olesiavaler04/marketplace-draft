from django.db import transaction

from .models import Order, OrderItem
from app_users.utils import change_user_status, change_user_balance


def check_balance(order_id: int) -> bool:
    """Проверяет, достаточно ли у пользователя средств для оплаты"""
    order_obj = Order.objects.select_related('user').get(id=order_id)
    total_price = order_obj.get_total_price()
    if order_obj.user.balance < total_price:
        return False
    return True


def change_product_quantity(order_id: int) -> None:
    """Изменяет количество товаров"""
    order_items = OrderItem.objects.prefetch_related('order').filter(order=order_id)
    for item in order_items:
        item.product.quantity -= item.quantity
        item.product.save()


def change_paid_order_status(order_id: int) -> None:
    """Изменяет статус заказа на: Оплачен"""
    order = Order.objects.select_related('user').get(id=order_id)
    order.paid = True
    order.save()


@transaction.atomic
def purchasing(user_id: int, order_id: int) -> None:
    """Транзакция совершения покупки.
        1) проверяет баланс пользователя
        2) уменьшает количество товара в магазине
        3) изменяет статус оплаты заказа
    """
    change_user_balance(user_id, order_id)
    change_product_quantity(order_id)
    change_paid_order_status(order_id)
