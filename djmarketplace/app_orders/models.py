from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from app_shops.models import Product
from app_users.models import Profile


class Order(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name=_('профиль пользователя'),
                             related_name='usr_order')
    first_name = models.CharField(max_length=50, verbose_name=_('имя'))
    last_name = models.CharField(max_length=50, verbose_name=_('фамилия'))
    email = models.EmailField(verbose_name=_('электронная почта'))
    city = models.CharField(max_length=100, verbose_name=_('город'))
    address = models.CharField(max_length=250, verbose_name=_('адрес'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('создан'))
    updated = models.DateTimeField(auto_now=True, verbose_name=_('обновлён'))
    paid = models.BooleanField(default=False, verbose_name=_('оплачен'))

    class Meta:
        ordering = ('-created',)
        verbose_name_plural = _('заказ')
        verbose_name = _('заказы')

    def __str__(self):
        return '{} {}'.format(_('Заказ'), self.pk)

    def get_total_price(self):
        return sum(item.get_price() for item in self.items.all())

    def get_absolute_url(self):
        return reverse('orders:order_page', kwargs={'order_id': self.pk})


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE, verbose_name=_('заказ'))
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE, verbose_name=_('товар'))
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_('цена'))
    quantity = models.PositiveIntegerField(default=1, verbose_name=_('количество'))

    def __str__(self):
        return '{}'.format(self.pk)

    def get_price(self):
        return self.price * self.quantity
