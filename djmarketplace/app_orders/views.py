from django.shortcuts import render, redirect
from django.views.generic import ListView

from .models import OrderItem, Order
from .forms import OrderCreateForm
from .utils import check_balance, purchasing
from app_cart.cart import Cart


class OrdersHistory(ListView):
    model = Order
    template_name = 'orders/orders_list.html'
    paginate_by = 5

    def get_context_data(self, *args, object_list=None, **kwargs):
        object_list = Order.objects.select_related('user').all()
        context = super().get_context_data(*args, **kwargs)
        context['orders_list'] = object_list
        return context


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
                cart.clear()
            return render(request, 'orders/order/created.html', {'order': order})
    else:
        initial_dict = {'user': request.user.profile.id,
                        'first_name': request.user.first_name,
                        'last_name': request.user.last_name,
                        'email': request.user.email,
                        'city': request.user.profile.city}
        form = OrderCreateForm(initial=initial_dict)
    return render(request, 'orders/order/create.html', {'cart': cart, 'form': form})


def order_page(request, order_id):
    if request.method == 'POST':
        if 'paid' in request.POST:
            if check_balance(order_id):
                purchasing(request.user.id, order_id)
                return redirect('account')
            else:
                return redirect('/top_up_balance')
        elif 'delete' in request.POST:
            Order.objects.filter(id=order_id).delete()
            return redirect('account')

    order = Order.objects.select_related('user').get(id=order_id)
    return render(request, 'orders/order_page.html', context={'order': order})
