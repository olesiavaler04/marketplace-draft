from django import forms
from django.forms import NumberInput
from django.utils.translation import gettext_lazy as _


class ReportForm(forms.Form):
    date_from = forms.DateTimeField(label=_('c'), widget=NumberInput(attrs={'type': 'date'}))
    date_to = forms.DateTimeField(label=_('до'), widget=NumberInput(attrs={'type': 'date'}))