import statistics
from random import choices
from typing import Union, List, Dict

from django.db.models import QuerySet

from .models import Promotions


def get_product_promo_for_account(number_of_promo: int) -> Union[List[QuerySet], QuerySet]:
    """Функция возвращает number_of_promo случайных акций, связанных с товарами"""
    promotions = Promotions.objects.prefetch_related('product').exclude(product__isnull=True)
    if len(promotions) < number_of_promo:
        return promotions
    else:
        return choices(promotions, k=number_of_promo)


def get_product_item_from_promo(promotions: QuerySet) -> Dict:
    """Функция создает и возвращает словарь, где ключ - объект акции,
    а значение - объект товара, который в ней участвует"""
    items = dict()
    for promo in promotions:
        items[promo] = promo.product.first()
    return items


def get_shop_item_from_promo(promotions: QuerySet) -> Dict:
    """Функция создает и возвращает словарь, где ключ - объект акции,
    а значение - объект магазина, который в ней участвует"""
    items = dict()
    for promo in promotions:
        items[promo] = promo.shop.first()
    return items


def get_shop_promo_for_account(number_of_promo: int) -> Union[List[QuerySet], QuerySet]:
    """Функция возвращает number_of_promo случайных акций, связанных с магазинами"""
    promotions = Promotions.objects.prefetch_related('shop').exclude(shop__isnull=True)
    if len(promotions) < number_of_promo:
        return promotions
    else:
        return choices(promotions, k=number_of_promo)


def get_exclusive_promo_for_account(number_of_promo: int) -> Union[List[QuerySet], QuerySet]:
    """Функция возвращает number_of_promo случайных эксклюзивных акций"""
    promotions = Promotions.objects.filter(exclusive=True)
    if len(promotions) < number_of_promo:
        return promotions
    else:
        return choices(promotions, k=number_of_promo)


def get_product_on_market_dict(products_list: QuerySet) -> Dict:
    """Функция создает и возвращает словарь, где ключ - товар маркетплейса,
    а значение - список магазинов, в котором продается данный товар."""
    prod_in_market = dict()
    for product in products_list:
        if product.name not in prod_in_market:
            prod_in_market[product.name] = [product.shop]
        else:
            prod_in_market[product.name].append(product.shop)
    return prod_in_market


def get_best_selling_products(order_list: QuerySet) -> Dict:
    """Функция принимает на вход список заказов за определенное время.
    Вычисляет, какие товары были самыми продаваемыми: вычисляется медиана количества проданных товаров и
    выбираются товары, количество которых в совокупности больше этого значения.
    Возвращается словарь, где ключ - объект товара, а значение - его проданное количество."""
    order_items = get_order_items(order_list)

    products = dict()
    for item in order_items:
        if item.product in products:
            products[item.product] += item.quantity
        else:
            products[item.product] = item.quantity

    prod_quantities = set(products.values())
    median_value = statistics.median(prod_quantities)

    best_products = dict()
    for product, quantity in products.items():
        if quantity > median_value:
            best_products[product] = quantity

    return best_products


def get_order_items(order_list: QuerySet) -> List:
    """Функция получает список заказов и возвращает список из товарных единиц всех заказов"""
    order_items = list()
    for order in order_list:
        for item in order.items.all():
            order_items.append(item)
    return order_items
