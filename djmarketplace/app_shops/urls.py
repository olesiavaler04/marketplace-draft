from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .views import main_page_view, shop_page_view, detail_product_view, ProductListView, report_view

app_name = 'shop'

urlpatterns = [
    path('', main_page_view),
    path('shop/<slug:shop_slug>', shop_page_view, name='shop'),
    path('shop/<int:shop_id>/product/<int:prod_id>', detail_product_view, name='product'),
    path('products/', ProductListView.as_view(), name='products_list'),
    path('report_best_sellers', report_view, name='report_bsp')
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
