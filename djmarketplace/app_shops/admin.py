from django.contrib import admin

from .models import Shop, Product, Promotions, Offers, StoreHouse


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'article', 'price', 'shop']


@admin.register(Promotions)
class PromotionsAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'discount_size']


@admin.register(Offers)
class OffersAdmin(admin.ModelAdmin):
    list_display = ['id', 'description']


@admin.register(StoreHouse)
class OffersAdmin(admin.ModelAdmin):
    list_display = ['id', 'address']
