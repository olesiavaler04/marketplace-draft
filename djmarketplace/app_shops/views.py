# from django.core import paginator
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import ListView

from app_cart.forms import CartAddProductForm
from app_orders.models import Order
from .models import Shop, Product, Promotions
from .utils import get_product_on_market_dict, get_best_selling_products
from .forms import ReportForm


def main_page_view(request):
    object_list = Shop.objects.all()
    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        shops = paginator.page(page)
    except PageNotAnInteger:
        shops = paginator.page(1)
    except EmptyPage:
        shops = paginator.page(paginator.num_pages)
    return render(request, 'index.html', {'page': page, 'shops': shops})


class ProductListView(ListView):
    model = Product
    template_name = 'product_list.html'
    paginate_by = 5

    def get_context_data(self, *args, products_list=None, **kwargs):
        products_list = Product.objects.select_related('shop').all()
        prod_in_market = get_product_on_market_dict(products_list)
        paginator = Paginator(products_list, self.paginate_by)
        context = super().get_context_data(*args, **kwargs)

        page = self.request.GET.get('page')
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        context['product_list'] = products
        context['product_dict'] = prod_in_market
        return context


def shop_page_view(request, shop_slug):
    shop = Shop.objects.get(slug=shop_slug)
    product_list = Product.objects.filter(shop=shop.id)
    return render(request, 'shop_detail.html', context={'shop': shop, 'product_list': product_list})


def detail_product_view(request, shop_id, prod_id):
    product = Product.objects.select_related('shop').get(id=prod_id)

    cart_product_form = CartAddProductForm()
    promo = Promotions.objects.filter(product=product.id)
    context = {
        'product': product,
        'promotions': promo,
        'cart_product_form': cart_product_form
    }
    return render(request, 'product_detail.html', context=context)


def report_view(request):
    context = {'form': ReportForm()}

    if request.method == 'POST':
        form = ReportForm(request.POST)
        if form.is_valid():
            date_from = form.cleaned_data['date_from']
            date_to = form.cleaned_data['date_to']
            orders = Order.objects.prefetch_related('user').filter(paid=True, created__gte=date_from,
                                                                   created__lte=date_to)
            if orders:
                bs_products = get_best_selling_products(orders)
                context['products'] = bs_products
                context['date_from'] = date_from
                context['date_to'] = date_to
            else:
                context['no_data'] = True

    return render(request, 'report_best_sellers.html', context=context)




