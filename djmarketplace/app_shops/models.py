from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.core.validators import MaxValueValidator


class StoreHouse(models.Model):
    address = models.CharField(max_length=200, verbose_name=_('адрес'))
    products = models.ManyToManyField('Product', verbose_name=_('товары'), blank=True)

    def __str__(self):
        return self.address

    class Meta:
        verbose_name_plural = _('склады')
        verbose_name = _('склад')


class Shop(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('название'))
    logo = models.ImageField(upload_to='logo/', blank=True, null=True, verbose_name=_('логотип'),
                             default='default/logo-default.png')
    store_house = models.OneToOneField(StoreHouse, on_delete=models.SET_NULL, null=True, blank=True,
                                       verbose_name=_('склад'))
    slug = models.SlugField(max_length=50, verbose_name=_('имя url'), unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:shop', kwargs={'shop_slug': self.slug})

    class Meta:
        verbose_name_plural = _('магазины')
        verbose_name = _('магазин')


class Product(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('товар'))
    article = models.CharField(max_length=10, verbose_name=_('артикул'))
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_('цена'))
    description = models.TextField(blank=True, null=True, verbose_name=_('описание'))
    shop = models.ForeignKey('Shop', on_delete=models.CASCADE, verbose_name=_('магазин'))
    quantity = models.PositiveIntegerField(default=0, verbose_name='остаток на складе')
    product_photo = models.ImageField(upload_to='products/', blank=True, null=True, verbose_name=_('фото товара'),
                                      default='default/product-default.png')

    def __str__(self):
        return f'{self.name}'

    def get_absolute_url(self):
        return reverse('shop:product', kwargs={'prod_id': self.pk, 'shop_id': self.shop.id})

    class Meta:
        verbose_name_plural = _('товары')
        verbose_name = _('товар')
        ordering = ['price']


class Promotions(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('название акции'))
    description = models.TextField(blank=True, null=True, verbose_name=_('описание'))
    picture = models.ImageField(upload_to='promo/', blank=True, null=True, verbose_name=_('рисунок'))
    shop = models.ManyToManyField(Shop, blank=True, verbose_name=_('магазин'))
    product = models.ManyToManyField(Product, blank=True, verbose_name=_('товар'), related_name='product')
    discount_size = models.PositiveIntegerField(validators=[MaxValueValidator(100)], blank=True, null=True,
                                                verbose_name=_('размер скидки'))
    exclusive = models.BooleanField(default=False, verbose_name=_('эксклюзивная'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = _('акции')
        verbose_name = _('акция')


class Offers(models.Model):
    description = models.TextField(blank=True, null=True, verbose_name=_('описание'))
    picture = models.ImageField(upload_to='promo/', blank=True, null=True, verbose_name=_('рисунок'))

    def __str__(self):
        return self.description

    class Meta:
        verbose_name_plural = _('предложения')
        verbose_name = _('предложение')
