from django.urls import path

from .views import *

urlpatterns = [
    path('login', BuyerLoginView.as_view(), name='login'),
    path('logout', BuyerLogoutView.as_view(), name='logout'),
    path('register', register_view, name='register'),
    path('account', account_view, name='account'),
    path('top_up_balance', top_up_balance_view, name='top_up_balance'),
    path('about_statuses', about_statuses_view, name='about_statuses'),
]
