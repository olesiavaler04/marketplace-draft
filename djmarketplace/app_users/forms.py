from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms.widgets import NumberInput
from django.utils.translation import gettext_lazy as _


class RegisterForm(UserCreationForm):
    date_of_birth = forms.DateField(label=_('Дата рождения'), widget=NumberInput(attrs={'type': 'date'}))
    city = forms.CharField(max_length=50, label=_('Город'))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'date_of_birth', 'city', 'email', 'password1', 'password2')


class BalanceForm(forms.Form):
    money = forms.DecimalField(label=_('Пополнить баланс'))
