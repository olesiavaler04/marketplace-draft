from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.core.cache import cache
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from .models import Profile
from .forms import RegisterForm, BalanceForm
from .utils import check_for_info_user_status, change_user_status
import app_shops.utils as utils
from app_orders.models import Order, OrderItem


@login_required
def account_view(request):
    product_promo_cache_key = f'product_promotions:{request.user.username}'
    shop_promo_cache_key = f'shop_promotions:{request.user.username}'
    offer_cache_key = f'offers:{request.user.username}'

    profile = Profile.objects.select_related('user').get(user=request.user)
    product_promotions = utils.get_product_promo_for_account(5)
    shop_promotions = utils.get_shop_promo_for_account(3)
    exclusive_promo = utils.get_exclusive_promo_for_account(3)
    offers = profile.offers.all()

    user_account_cache_data = {
        product_promo_cache_key: product_promotions,
        shop_promo_cache_key: shop_promotions,
        offer_cache_key: offers
    }
    cache.set_many(user_account_cache_data)

    change_user_status(profile.id)

    context = {
        'profile': profile,
        'shop_items': utils.get_shop_item_from_promo(shop_promotions),
        'product_items': utils.get_product_item_from_promo(product_promotions),
        'exclusive_promo': exclusive_promo,
        'offers': offers,
        'about_status': check_for_info_user_status(profile)
    }

    orders = Order.objects.prefetch_related('user').filter(user=request.user.profile.id)
    if orders:
        unpaid_orders = Order.objects.prefetch_related('user').filter(paid=False)
        if unpaid_orders:
            context['unpaid_orders'] = unpaid_orders
        else:
            last_order = Order.objects.prefetch_related('user').first()
            context['last_order'] = OrderItem.objects.prefetch_related('order').filter(order=last_order.id)
    else:
        context['orders_count'] = 0

    return render(request, 'app_users/account.html', context=context)


class BuyerLoginView(LoginView):
    template_name = 'app_users/login.html'


class BuyerLogoutView(LogoutView):
    template_name = 'app_users/logout.html'


def register_view(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            city = form.cleaned_data.get('city')
            date_of_birth = form.cleaned_data.get('date_of_birth')
            Profile.objects.create(user=user, city=city, date_of_birth=date_of_birth)
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/account')
        else:
            return redirect('/')
    else:
        form = RegisterForm()
    return render(request, 'app_users/register.html', {'form': form})


@login_required
def top_up_balance_view(request):
    if request.method == 'POST':
        form = BalanceForm(request.POST)
        if form.is_valid():
            user = Profile.objects.get(user=request.user)
            user.balance += form.cleaned_data.get('money')
            user.save()
            return HttpResponseRedirect('account')
    else:
        form = BalanceForm()

    return render(request, 'app_users/balance.html', {'form': form})


def about_statuses_view(request):
    return render(request, 'app_users/about_statuses.html')
