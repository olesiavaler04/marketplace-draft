
from django.utils.translation import gettext_lazy as _

from .models import Profile
from app_orders.models import Order


def check_for_info_user_status(user: Profile) -> str:
    """Функция проверяет статус пользователя и возвращает строку с информацией о нём"""
    pattern = _('Для получения статуса "{}" необходимо совершить покупок на сумму: {}р.')
    if user.status_name == 'b':
        return pattern.format(_('Серебряный'), str(10000 - round(user.status_balance)))
    elif user.status_name == 's':
        return pattern.format(_('Золотой'), str(100000 - round(user.status_balance)))
    elif user.status_name == 'g':
        return pattern.format(_('Платиновый'), str(500000 - round(user.status_balance)))
    elif user.status_name == 'p':
        return _('У Вас самый высокий статус.')


def change_user_status(user_id: int):
    """Функция проверяет и изменяет статус пользователя"""
    user = Profile.objects.select_related('user').get(id=user_id)

    if user.status_balance >= 500000:
        user.status_name = 'p'
    elif user.status_balance >= 100000:
        user.status_name = 'g'
    elif user.status_balance >= 10000:
        user.status_name = 's'

    user.save()


def change_user_balance(user_id: int, order_id: int) -> None:
    """Функция уменьшает денежный баланс и увеличивает бонусный баланс пользователя"""
    profile = Profile.objects.select_related('user').get(user=user_id)
    order = Order.objects.select_related('user').get(id=order_id)
    total_price = order.get_total_price()
    profile.balance -= total_price
    profile.status_balance += total_price  # пока без углубления в бонусную программу
    profile.save()
