from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _

from app_shops.models import Offers


class Profile(models.Model):
    STATUS_CHOICES = (
        ('b', _('Базовый')),
        ('s', _('Серебряный')),
        ('g', _('Золотой')),
        ('p', _('Платиновый'))
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True, verbose_name=_('дата рождения'))
    city = models.CharField(max_length=50, verbose_name=_('город'))
    balance = models.DecimalField(default=0, max_digits=10, decimal_places=2, verbose_name=_('баланс'))
    offers = models.ManyToManyField(Offers, blank=True, verbose_name=_('предложения'),
                                    related_name='usr_offer')
    status_balance = models.DecimalField(default=0, max_digits=10, decimal_places=2, verbose_name=_('сумма покупок'))
    status_name = models.CharField(max_length=1, default='b', choices=STATUS_CHOICES, verbose_name=_('статус'))

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = _('профили')
        verbose_name = _('профиль')
