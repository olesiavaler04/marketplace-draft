FROM python:3.10

COPY . /code

RUN pip install -r /code/requirements.txt

WORKDIR /code/djmarketplace

RUN python manage.py migrate

RUN python manage.py compilemessages

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
